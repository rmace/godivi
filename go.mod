module richardmace.co.uk/boxdivi

go 1.14

require (
	github.com/AlecAivazis/survey/v2 v2.1.1
	github.com/bmizerany/pat v0.0.0-20170815010413-6226ea591a40
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/dustin/go-humanize v1.0.0
	github.com/gizak/termui/v3 v3.1.0
	github.com/golangcollege/sessions v1.2.0
	github.com/inconshreveable/go-update v0.0.0-20160112193335-8152e7eb6ccf
	github.com/justinas/alice v1.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/go-ps v1.0.0
	github.com/richardltc/gwcommon v0.0.0-20200805124143-fe68451bd1e1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	github.com/theckman/yacspin v0.8.0
)
