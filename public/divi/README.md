# GoDivi #

GoDivi is a CLI application that makes it very easy to set up and view a Divi wallet by just using the command line on the platform of your choosing, with a single command.

## What is GoDivi used for? ###

* Making the initial installation of a Divi wallet trivial with the single command `./godivi-install install`
* Web enabling your existing wallet.
* Displaying the status of your wallet. 
* Displaying blockchain and master node syncing status
* Displaying of required coins for staking.
* Auto fixing common wallet issues.

### How do I run? ###

* Download the zip file, extract the files and simply run it: `./godivi-install install`

This will download the official binaries from the Divi Project website, and installs the GoDivi CLI and Updater apps.

Then, change to the `godivi` folder in your home folder, and run `./godivi start`, to start the `divid` server and then you can run  `./godivi dash`, where you should be prompted through an initial wizard to get you going.

### OK, but what else can GoDivi do? ###


* Creates an initial wallet from scratch

* Takes you through an initial wizard to ensure your wallet is secure.

* `godivi-installer install` or `godivi install` - Installs the official Divi project CLI files, and creates a new wallet.

* `godivi dash` - Displays blockchain, syncing, staking and Divi balance info. (coming soon...)

* `godivi wallet encrypt/unlock/unlockfs` - (coming soon...) Allows the encryption and unlocking of the wallet for safe staking.

* Allows you to "web enable" your existing wallet.

## How do I setup a client/server environment?

There may be times when you have your `divid` server running on one machine (machine A), and you want to run GoDivi on another machine (machine B). This is how you would achieve that.

### Configure machine A (The `divid` server)

On the machine running the `divid` server, edit the `divi.conf` file, that's stored in the hidden folder `~/.divi/` and make sure it conatins the following settings, which are explained below:

```
rpcuser=divirpc
rpcpassword=A_Random_Password

server=1
rpcallowip=192.168.1.0/255.255.255.0
rpcport=51473

```
The `server=1` tells `divid` to listen for requests from GoDivi.

The `rpcallowip=192.168.1.0/255.255.255.0` line, tells the `divid` server what IP addresses to *allow* a connection from. So, in our example, any local ipaddress in the range of 192.168.1.1-254 would be able to connect to the `divid` server.

Finally, the `rpcport=51473` tells the `divid` server what port to listen on. e.g. `51473`

After the settings have been implemented, you'll need to restart your `divid` server which can be achieved by `./godivi stop` and then `./godivi start`.

### Configure machine B (The GoDivi client)

Download the latest version of GoDivi from the website, or just copy the files from an existing installation.

Edit the `cli.yml` config file, and make sure the following settings exist:
```
port: "51473"
rpcpassword: A_Random_Password
rpcuser: divirpc
serverip: 127.0.0.1

```

Make sure the `port`,`rpcuser` and `rpcpassword` are all the same as what it is in your `divi.conf` file on the server, and make sure the `serverip:` address, is set to be the same as the ipaddress as the server (machine A). In the above example, the `serverip: 127.0.0.1` would only work if GoDivi was running on the *same* server as `divid`.

## Is it free? ##

Yes! GoDivi is FREE to use, however, if you'd like to send a tip, please send Divi to the following Divi address:

DSniZmeSr62wiQXzooWk7XN4wospZdqePt

## Thank you for trying GoDivi